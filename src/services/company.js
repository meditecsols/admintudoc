import { http } from "./config";
import { getToken } from "@/utils/auth";

export default {
  getCompanies: () => {
    var token_bearer = "Bearer " + getToken();

    return http
      .get("company/", {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  getSubscriptons: () => {
    var token_bearer = "Bearer " + getToken();

    return http.get("subscriptions/", {
      headers: { Authorization: `${token_bearer}` }
    });
  },

  getEmployees: () => {
    var token_bearer = "Bearer " + getToken();
    return http.get("subscriptions/", {
      headers: { Authorization: `${token_bearer}` }
    });
  },

  getDoctores: () => {
    var token_bearer = "Bearer " + getToken();
    return http.get("patient/", {
      headers: { Authorization: `${token_bearer}` }
    });
  },

  getConsultations: () => {
    var token_bearer = "Bearer " + getToken();
    return http.get("consultation/", {
      headers: { Authorization: `${token_bearer}` }
    });
  },

  getDashoard: () => {
    var token_bearer = "Bearer " + getToken();
    return http.get("staff/dashboard/", {
      headers: { Authorization: `${token_bearer}` }
    });
  }
};
