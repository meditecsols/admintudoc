import { http, http_token } from "./config";
import { getToken } from "@/utils/auth";

export default {
    getServices: () => {
      var token_bearer = "Bearer " + getToken();
  
      return http
        .get("service/", {
          headers: { Authorization: `${token_bearer}` }
        })
        .catch(function(error) {
          return "An error occured.." + error;
        });
    }
  };