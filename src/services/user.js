import { http, http_token } from "./config";
import { getToken } from "@/utils/auth";

export default {
  login: userInfo => {
    return http.post("login/", userInfo).catch(function(error) {
      return "An error occured.." + error;
    });
  },
  token: (tokenInfo, datainfo) => {
    var tokenbase64 = window.btoa(
      `${datainfo.data.client_id}:${datainfo.data.client_secret}`
    );

    return http_token
      .post("token/", tokenInfo, {
        headers: { Authorization: `Basic ${tokenbase64}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  getUser: () => {
    var token_bearer = "Bearer " + getToken();

    return http
      .get("staff/", {
        headers: { Authorization: `${token_bearer}` },
        params: {
          token: getToken()
        }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  }
};
