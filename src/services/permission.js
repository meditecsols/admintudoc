import { getToken, setToken, removeToken } from "@/utils/auth";
export default {
  isLogged: context => {
    var token = getToken();
    if (token === undefined) {
      context.$router.push({ path: "/" });
      console.log("Token:" + getToken());
      return false;
    } else {
      //removeToken();

      context.$router.push({ path: "/dashboard" });
      console.log("Go to dashboard");
      return true;
    }
  },
  logOut: context => {
    removeToken();
    context.User = null;
    context.$router.push({ path: "/" });
  }
};
