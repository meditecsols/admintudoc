import { http, http_token } from "./config";
import { getToken } from "@/utils/auth";

export default {
  getPatients: () => {
    var token_bearer = "Bearer " + getToken();

    return http
      .get("patient/", {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  getPatientsById: (id) => {
    var token_bearer = "Bearer " + getToken();

    return http
      .get("patient/"+id+"/", {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  getPatientsByDoc: (id) => {
    var token_bearer = "Bearer " + getToken();

    return http
      .get("patient/?doctor="+id, {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },

  editPatient: (patient, object) => {
    var token_bearer = "Bearer " + getToken();

    return http
      .patch("patient/" + patient.id + "/", object, {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  createPatient: (patient)=>{

    var token_bearer = "Bearer " + getToken();

    return http
      .post("patient/", patient, {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return error.response;
      });

  },
  createPatientmetrics: (metric)=>{
    var token_bearer = "Bearer " + getToken();
    
    return http.post("patientmetrics/",metric,{

      headers: {Authorization: `${token_bearer}`}

    }).catch(function(error){
      return error.response
    });
  }
};
