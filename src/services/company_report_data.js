import moment from "moment";
export default {
  get_companies_date_started: response => {
    var ene = [];
    var feb = [];
    var mar = [];
    var abr = [];
    var may = [];
    var jun = [];
    var jul = [];
    var ago = [];
    var sep = [];
    var oct = [];
    var nov = [];
    var dic = [];

    for (var x in response.data) {
      let date = moment(response.data[x].start_date);
      var n = date.format("MM");

      switch (n) {
        case "01":
          ene.push(n);
          break;
        case "02":
          feb.push(n);
          break;
        case "03":
          mar.push(n);
          break;
        case "04":
          abr.push(n);
          break;
        case "05":
          may.push(n);
          break;
        case "06":
          jun.push(n);
          break;
        case "07":
          jul.push(n);
          break;
        case "08":
          ago.push(n);
          break;
        case "09":
          sep.push(n);
          break;
        case "10":
          oct.push(n);
          break;
        case "11":
          nov.push(n);
          break;
        case "12":
          dic.push(n);
          break;
      }

      var arrayMonthsValues = [];

      for (var i = 0; i < 12; i++) {
        switch (i) {
          case 0:
            arrayMonthsValues.push(ene.length);
            break;
          case 1:
            arrayMonthsValues.push(feb.length);
            break;
          case 2:
            arrayMonthsValues.push(mar.length);
            break;
          case 3:
            arrayMonthsValues.push(abr.length);
            break;
          case 4:
            arrayMonthsValues.push(may.length);
            break;
          case 5:
            arrayMonthsValues.push(jun.length);
            break;
          case 6:
            arrayMonthsValues.push(jul.length);
            break;
          case 7:
            arrayMonthsValues.push(ago.length);
            break;
          case 8:
            arrayMonthsValues.push(sep.length);
            break;
          case 9:
            arrayMonthsValues.push(oct.length);
            break;
          case 10:
            arrayMonthsValues.push(nov.length);
            break;
          case 11:
            arrayMonthsValues.push(dic.length);
            break;
        }
      }
    }

    return arrayMonthsValues;
  },
  get_num_companies: response => {
    return response.data.length;
  },
  get_num_consultas: response => {
    return response.data.length;
  },
  get_num_patients: response => {
    return response.data.length;
  }
};
