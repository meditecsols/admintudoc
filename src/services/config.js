import axios from "axios";

const API_URL = 'https://sandbox.family-doc.com/'
// const API_URL = "http://192.168.0.11:8000/";
//  const API_URL = "http://192.168.1.64:8000/";
// const API_URL = "https://api.family-doc.com/";

const API_V = "api/v0/";
const URL = `${API_URL}${API_V}`;
const TOKEN_URL = `${API_URL}o/`;

export const http = axios.create({
  baseURL: URL
});
export const http_token = axios.create({
  baseURL: TOKEN_URL
});
