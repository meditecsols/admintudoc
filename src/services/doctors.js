import { http, http_token } from "./config";
import { getToken } from "@/utils/auth";

export default {
    getDoctors: () => {
      var token_bearer = "Bearer " + getToken();
  
      return http
        .get("doctor/", {
          headers: { Authorization: `${token_bearer}` }
        })
        .catch(function(error) {
          return "An error occured.." + error;
        });
    },
    getDoctorsavailability: (id,cadenaFecha,cadenaTZ) => {
        var token_bearer = "Bearer " + getToken();
    
        return http
          .get("doctor/"+id+"/availability/?date="+cadenaFecha+"&tz="+cadenaTZ, {
            headers: { Authorization: `${token_bearer}` }
          })
          .catch(function(error) {
            return "An error occured.." + error;
          });
    },
    editDoctor: (doctor, object) => {
      var token_bearer = "Bearer " + getToken();
  
      return http
        .patch("doctor/" + doctor.id + "/", object, {
          headers: { Authorization: `${token_bearer}` }
        })
        .catch(function(error) {
          return "An error occured.." + error;
        });
    },
};