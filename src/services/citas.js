import { http, http_token } from "./config";
import { getToken } from "@/utils/auth";

export default {
  createCitas: (envio) => {
    var token_bearer = "Bearer " + getToken();

    return http
      .post("appointment/", envio,{
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  getCitas: () => {
    var token_bearer = "Bearer " + getToken();

    return http
      .get("appointment/allappointments/", {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  },
  getCitasofDay: () => {
    var token_bearer = "Bearer " + getToken();

    var day = new Date().getDate()
    var month = new Date().getMonth() + 1
    var year = new Date().getFullYear()

    return http
      .get("appointment/recentappointments/?"+"day="+day+"&month="+month+"&year="+year, {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return "An error occured.." + error;
      });
  }
};
