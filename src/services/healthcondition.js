import { http, http_token } from "./config";
import { getToken } from "@/utils/auth";

export default {
  
  createHealthCondition: (dictionary)=>{

    var token_bearer = "Bearer " + getToken();

    return http
      .post("healthcondition/",dictionary, {
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return error.response;
      });

  },
  patchHealthCondition: (id,dictionary)=>{

    var token_bearer = "Bearer " + getToken();

    return http
      .patch("healthcondition/"+id+"/",dictionary,{
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return error;
      });

  },
  getHealthCondition: (id)=>{

    var token_bearer = "Bearer " + getToken();

    return http
      .get("healthcondition/?patient="+id,{
        headers: { Authorization: `${token_bearer}` }
      })
      .catch(function(error) {
        return error;
      });

  }
};