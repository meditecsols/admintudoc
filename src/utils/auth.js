import Cookies from "js-cookie";

const TokenKey = "Admin-Token";
const UserEmail = "User-Email";
const UserPassword = "User-Password";

export function getToken() {
  return Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token);
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}

//Set, Get and Remove Email Sessión

export function getUserEmail() {
  return Cookies.get(UserEmail);
}

export function setUserEmail(email) {
  return Cookies.set(UserEmail, email);
}

export function removeEmail() {
  return Cookies.remove(UserEmail);
}

//Set, Get and Remove Password Sessión

export function getUserPassword() {
  return Cookies.get(UserPassword);
}

export function setUserPassword(password) {
  return Cookies.set(UserPassword, password);
}

export function removePassword() {
  return Cookies.remove(UserPassword);
}
