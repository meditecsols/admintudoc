// =========================================================
// * Vue Material Dashboard - v1.2.1
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vue-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
// * Licensed under MIT (https://github.com/creativetimofficial/vue-material-dashboard/blob/master/LICENSE.md)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";

import VueSimpleAlert from "vue-simple-alert";
import VueRouter from "vue-router";
import App from "./App";
import axios from "axios";
import VueAxios from "vue-axios";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'vue-search-select/dist/VueSearchSelect.css'
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueQrcode from '@chenfengyuan/vue-qrcode';
import vueCountryRegionSelect from 'vue-country-region-select'


locale.use(lang)
// router setup
import routes from "./routes/routes";

// Plugins
import GlobalComponents from "./globalComponents";
import GlobalDirectives from "./globalDirectives";
import Notifications from "./components/NotificationPlugin";

// MaterialDashboard plugin
import MaterialDashboard from "./material-dashboard";

// Token
import { getToken } from "@/utils/auth";

import Chartist from "chartist";

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkExactActiveClass: "nav-item active"
});

Vue.prototype.$Chartist = Chartist;

Vue.use(VueRouter);
Vue.use(MaterialDashboard);
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(Notifications);
Vue.use(VueAxios, axios);
Vue.use(VueSimpleAlert);
Vue.use(BootstrapVue);
Vue.component(VueQrcode.name, VueQrcode);
Vue.use(vueCountryRegionSelect)


Vue.config.productionTip = false;

Vue.store = Vue.prototype.User = {
  token: getToken(),
  name: "",
  avatar: "",
  roles: [],
  companyname: "",
  companypicurl: "",
  leftbeneficiaries: {},
  totalcountbeneficiaries: 0,
  totalmaxbeneficiaries: 0,
  totalcountsubscriptions: 0,
  totalcountconsultations: 0
};
Vue.store = Vue.prototype.graphicCompany = [];

/* eslint-disable no-new */
new Vue({
  el: "#app",
  render: h => h(App),
  router,
  data: {
    Chartist: Chartist
  }
});
