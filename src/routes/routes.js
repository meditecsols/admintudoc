import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import CitasList from "@/pages/CitasList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";
import login from "@/pages/Login/login.vue";
import CreatePatient from "@/pages/UserProfile/CreatePatient.vue";
import RecordPatient from "@/pages/UserProfile/RecordPatient.vue";
import NewCitas from "@/pages/NewCitas.vue";
import Doctors from "@/pages/DoctorsList.vue";
import RecordPatientApp from "@/pages/ExternalPage/RecordPatientApp";

const routes = [
  { path: "/login", component: login, hidden: true },
  { path: "/recordpatientapp", component: RecordPatientApp, hidden: false },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/login",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard
      },
      {
        path: "user",
        name: "User Profile",
        component: UserProfile
      },
      {
        path: "table",
        name: "Pacientes",
        component: TableList
      },
      {
        path: "typography",
        name: "Typography",
        component: Typography
      },
      {
        path: "icons",
        name: "Icons",
        component: Icons
      },
      {
        path: "maps",
        name: "Maps",
        meta: {
          hideFooter: true
        },
        component: Maps
      },
      {
        path: "notifications",
        name: "Notifications",
        component: Notifications
      },
      {
        path: "upgrade",
        name: "Upgrade to PRO",
        component: UpgradeToPRO
      },
      {
        path: "createpatient",
        name: "Nuevo Paciente",
        component: CreatePatient
      }
      ,
      {
        path: "citaslist",
        name: "Citas",
        component: CitasList
      }
      ,
      {
        path: "recordpatient",
        name: "Expediente",
        component: RecordPatient
      },
      {
        path: "newcita",
        name: "Crear Cita",
        component: NewCitas
      },

      {
        path: "doctorslist",
        name: "Doctores",
        component: Doctors
      }
    ]
  }
];

export default routes;
